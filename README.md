# Bidding using Diversified Search Algorithms

This repository is maintained by _**Thimjo Koça**_ to supplement the article "Search Algorithms to Enable Negotiating Agents Explore Very Large Outcome Spaces" published at *Annals of Mathematics and Artificial Intelligence*. In contains:
- The source code of BIDS (**Bi**dding using **D**iversified **S**earch) algorithm.
- The source code of Sampling-BIDS algorithm.

> Koça Th., de Jonge D., Baarslag T. (2023). Search Algorithms to Enable Negotiating Agents Explore Very Large Outcome Spaces. *Annals of Mathematics and Artificial Intelligence*
