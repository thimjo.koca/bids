package utility_sampling;

import commons.Bid;

public interface UtilitySampling {
    public abstract Bid getBidAboveUtility(double uThreshold);

}
