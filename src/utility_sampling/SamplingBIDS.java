package utility_sampling;

import java.util.Random;

import commons.Bid;
import commons.LinearUtilityFunction;
import utility_lookup.BIDS;

public class SamplingBIDS implements UtilitySampling {
    private BIDS BIDS;
    private Random rand;

    public SamplingBIDS(LinearUtilityFunction u, int uPrecision, long seed) {
        BIDS = new BIDS(u, uPrecision);
        rand = new Random(seed);
    }

    @Override
    public Bid getBidAboveUtility(double uThreshold) {
        double dU = 1. - uThreshold,
                uTarget = uThreshold + dU * rand.nextDouble();

        return BIDS.getBidNearUtility(uTarget);
    }

}
