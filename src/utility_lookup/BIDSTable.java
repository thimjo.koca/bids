package utility_lookup;

import java.util.*;

import commons.Bid;
import commons.LinearUtilityFunction;

/**
 * A class that implements the dynamic programming table used by utility_lookup.BIDS,
 * the algorithm that tackles the utility-lookup query:
 * argmax_{\omega \in \Omega} |u(\omega) - u_t|
 *
 * @author thimjo
 *
 */
public class BIDSTable {
    /**
     * UtilityLookupTable is implementated through a mapping of mappings where:
     * - The outer key is the latest issue name over which the (partial)
     *   bids of its entries are defined.
     * - The inner key is an integer representation of the utility
     *   target/threshold for which the entry bid is optimal.
     */
    protected Map<String, Map<Integer, Bid>> dPTable;

    /**
     * It specifies the numerical precision (i.e. the number of decimal digits)
     * of the entries in the dynamic programming table.
     */
    final protected int precision;

    private List<String> usedIssues;

    public BIDSTable(LinearUtilityFunction uF, int precision) {
        this.precision = precision;
        this.usedIssues = populateTable(uF);
    }

    private List<String> populateTable(LinearUtilityFunction uF) {
        this.dPTable = new HashMap<>();

        List<String> issuesUsed = new ArrayList<>();
        for(String issue: uF.issues()) {
            this.dPTable.put(issue, new HashMap<>());
            fillTableEntriesUsingNIssues(issuesUsed, issue, uF);
            issuesUsed.add(issue);
        }

        return issuesUsed;
    }

    private void fillTableEntriesUsingNIssues(List<String> issuesUsed,
                                              String extraIssue,
                                              LinearUtilityFunction uF) {
        issuesUsed.add(extraIssue);
        List<Double> uTargets = genUtilityTargets(issuesUsed, uF);
        issuesUsed.remove(extraIssue);

        for(double uTarget: uTargets) {
            Bid closestBid = findClosest(issuesUsed, extraIssue, uF, uTarget);
            put(extraIssue, uTarget, closestBid);
        }
    }

    /**
     * Generates the utility targets to be included in the dynamic
     * programming table when a specified list of issues is used.
     * @param issuesUsed
     * @param uF
     * @return
     */
    private List<Double> genUtilityTargets(List<String> issuesUsed, LinearUtilityFunction uF) {
        List<Double> targets = new ArrayList<Double>();
        double u =0.,
                maxPossibleUtil = round(uF.getMaxPossibleUtilWith(issuesUsed)),
                uStep = Math.pow(10, -precision);
        while(u <= maxPossibleUtil) {
            targets.add(u);
            u += uStep;
            u = round(u);
        }

        return targets;
    }

    private double round(double u) {
        double pow10 = Math.pow(10, precision);
        double roundedU = Math.round(u*pow10) / pow10;
        return roundedU;
    }

    /**
     * Puts a (partial) bid in the dynamic-programming table.
     * @param issue -> the latest issue for which the partial bid is defined.
     * @param utilTarget -> the utility target for which the partial bid is (approximately) optimal.
     * @param bid -> the (partial) bid to put in the table.
     */
    private void put(String issue, double utilTarget, Bid bid) {
        int utilTargetKey = makeKey(utilTarget);
        if(! this.dPTable.containsKey(issue))
            this.dPTable.put(issue, new HashMap<>());

        this.dPTable.get(issue).put(utilTargetKey, bid);
    }

    /**
     * Transforms a utility target to an integer key in the following way:
     * 1. The utility target is rounded.
     * 2. The rounded utility is multiplied by the corresponding power
     *    of 10 and transformed to an integer.
     * @param utilTarget
     * @return
     */
    protected int makeKey(double utilTarget) {
        double pow10 = Math.pow(10, precision);
        int intTarget = (int) Math.round(utilTarget*pow10);
        return intTarget;
    }

    private Bid findClosest(List<String> issuesUsed, String extraIssue, LinearUtilityFunction uF, double uTarget) {
        double db = Double.MAX_VALUE;
        Bid best=null;
        for(String value: uF.getPossibleValues(extraIssue)) {
            Bid singleIssueBid = new Bid(extraIssue, value);

            double singleIssueBidUtil = uF.getUtility(singleIssueBid),
                    partialTarget = determineClosestPossibleUtilTarget(
                            uF,
                            issuesUsed,
                            uTarget - singleIssueBidUtil);

            Bid partialBid = getBid(issuesUsed, partialTarget);
            double utilCandidate = uF.getUtility(partialBid)
                    + singleIssueBidUtil,
                    dc = Math.abs(uTarget - utilCandidate);

            if(dc < db) {
                db = dc;
                best = new Bid(partialBid, extraIssue, value);
            }
        }

        return best;
    }

    /**
     *
     * @param issuesUsed
     * @param utilTarget
     * @return the (approximately) optimal partial bid, defined over the specified list of issues, for the given utility target.
     */
    private Bid getBid(List<String> issuesUsed, double utilTarget) {
        if(issuesUsed.size() == 0)
            return Bid.EMPTY_BID;

        String latestIssue = issuesUsed.get(issuesUsed.size()-1);
        int utilTargetKey = makeKey(utilTarget);
        return dPTable.get(latestIssue).get(utilTargetKey);
    }

    public double determineClosestPossibleUtilTarget(LinearUtilityFunction uF, List<String> usedIssues, double uTarget) {
        uTarget = Math.max(uTarget, 0.);
        double uMax = uF.getMaxPossibleUtilWith(usedIssues);
        return Math.min(uTarget, uMax);
    }

    public TreeMap<Double, Bid> getBidsPerULevel(LinearUtilityFunction uF) {
        TreeMap<Double, Bid> bids = new TreeMap<>();
        List<Double> utilTargets = genUtilityTargets(usedIssues, uF);
        for(Double utilTarget : utilTargets)
            bids.put(utilTarget, getBid(usedIssues, utilTarget));
        return bids;
    }

}
