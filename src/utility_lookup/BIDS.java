package utility_lookup;

import commons.Bid;
import commons.LinearUtilityFunction;

import java.util.Map;
import java.util.TreeMap;

/**
 * Class that implements the utility_lookup.BIDS algorithm to tackle the utility query.
 * @author thimjo
 *
 */
public class BIDS extends UtilityLookup {
    private TreeMap<Double, Bid> solutionsPerULevel;
    private final int uPrecision;

    public BIDS(LinearUtilityFunction u, int uPrecision) {
        super(u);
        this.uPrecision = uPrecision;
        initSolutionsPerULevel();
    }

    private void initSolutionsPerULevel() {
        BIDSTable dPTable = new BIDSTable(u, uPrecision);
        this.solutionsPerULevel = dPTable.getBidsPerULevel(u);
    }

    @Override
    public Bid getBidNearUtility(double uTarget) {
        Map.Entry<Double, Bid> low = solutionsPerULevel.floorEntry(uTarget),
                high = solutionsPerULevel.ceilingEntry(uTarget);

        if(low == null)
            return high.getValue();
        if(high == null)
            return low.getValue();

        double dLow = uTarget - low.getKey(),
                dHigh = high.getKey() - uTarget;

        return (dLow < dHigh) ? low.getValue() : high.getValue();
    }

}
