package utility_lookup;

import java.util.ArrayList;
import java.util.List;

import commons.Bid;
import commons.LinearUtilityFunction;

public abstract class UtilityLookup {
    protected LinearUtilityFunction u;

    public UtilityLookup(LinearUtilityFunction u) {
        this.u = u;
    }

    public abstract Bid getBidNearUtility(double uTarget);

    public List<Double> variability(List<Bid> bidsPerTarget) {
        List<Double> result = new ArrayList<>();
        result.add(0.);
        for(int i=1; i < bidsPerTarget.size(); i++) {
            double v = u.variability(bidsPerTarget.get(i), bidsPerTarget.get(i-1));
            result.add(v);
        }

        return result;
    }

}
