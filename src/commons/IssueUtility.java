package commons;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

public class IssueUtility {
    private Map<String, Double> issueUtilPerValue;
    private Random rand;

    public IssueUtility(List<String> values, boolean randParams, long seed) {
        rand = new Random(seed);
        initPossibleIssueUtilities(values, randParams);

    }

    private void initPossibleIssueUtilities(List<String> values, boolean randValues) {
        issueUtilPerValue = new HashMap<>();
        double uStep = 1./values.size(),
                u = 0.;
        for(String value : values)
            if(randValues)
                issueUtilPerValue.put(value, rand.nextDouble());
            else {
                issueUtilPerValue.put(value, u);
                u += uStep;
            }
    }

    public double getUtility(String value) {
        return issueUtilPerValue.get(value);
    }

    public String getMaxValue() {
        return issueUtilPerValue.entrySet()
                .stream()
                .max((Entry<String, Double> e1, Entry<String, Double> e2)
                        -> e1.getValue().compareTo(e2.getValue()))
                .get()
                .getKey();
    }

    public String getValueWithClosestUtility(double uTarget) {
        return issueUtilPerValue.keySet()
                .stream()
                .reduce((v1, v2) ->
                        (Math.abs(uTarget - getUtility(v1))
                                < Math.abs(uTarget - getUtility(v2))
                                ? v1 : v2)).get();
    }

}