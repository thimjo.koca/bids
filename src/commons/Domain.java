package commons;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Domain implements Iterable<String> {
    private List<String> issues;
    private Map<String, List<String>> valuesPerIssue;
    private Random rand;

    public Domain(int nIssues, int nValues, long seed) {
        rand = new Random(seed);
        issues = generateIssues(nIssues);
        valuesPerIssue = generateValues(nValues);
    }

    private List<String> generateIssues(int nIssues) {
        List<String> issues = new ArrayList<String>();
        for(int i=1; i<=nIssues; i++)
            issues.add("issue" + i);

        return issues;
    }

    private Map<String, List<String>> generateValues(int nValues) {
        Map<String, List<String>> valuesPerIssues = new HashMap<>();
        for(int i=0; i<issues.size(); i++) {
            String issue = issues.get(i);
            List<String> valuesPerIssue = generateValuesPerIssue(i+1,
                    nValues);
            valuesPerIssues.put(issue, valuesPerIssue);
        }

        return valuesPerIssues;
    }

    private List<String> generateValuesPerIssue(int issueIndex, int nValues) {
        List<String> values = new ArrayList<>();
        for(int i=1; i<=nValues; i++)
            values.add("value" + issueIndex + "_" + i);

        return values;
    }

    public List<String> getIssues(){
        return Collections.unmodifiableList(issues);
    }

    public List<String> getValues(String issue) {
        return new ArrayList<>(valuesPerIssue.get(issue));
    }

    public Bid sample() {
        Map<String, String> bid = new HashMap<>();
        for(String issue: issues) {
            List<String> values = valuesPerIssue.get(issue);
            int iValue = rand.nextInt(values.size());
            bid.put(issue, values.get(iValue));
        }

        return new Bid(bid);
    }

    @Override
    public Iterator<String> iterator() {
        return issues.iterator();
    }

}