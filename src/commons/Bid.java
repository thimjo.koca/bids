package commons;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Bid implements Iterable<String> {
    public static Bid EMPTY_BID = new Bid(new HashMap<>());

    public Map<String, String> bid;

    public Bid(Map<String, String> bid) {
        this.bid = bid;
    }

    public Bid(Bid partialBid, String extraIssue, String extraValue) {
        bid = new HashMap<String, String>();
        for(String issue: partialBid)
            bid.put(issue, partialBid.getValue(issue));
        bid.put(extraIssue, extraValue);
    }

    public Bid(String issue, String value) {
        bid = new HashMap<String, String>();
        bid.put(issue, value);
    }

    public String getValue(String issue) {
        return bid.get(issue);
    }

    @Override
    public Iterator<String> iterator() {
        return bid.keySet().iterator();
    }

    @Override
    public String toString() {
        return bid.toString();
    }

    @Override
    public boolean equals(Object other) {

        if( ! (other instanceof Bid)) {
            return false;
        }

        Bid otherBid = (Bid) other;

        return this.bid.equals(otherBid.bid);
    }

    @Override
    public int hashCode() {
        return this.bid.hashCode();
    }

}