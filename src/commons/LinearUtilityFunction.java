package commons;

import java.util.*;

public class LinearUtilityFunction implements Comparator<Bid> {
    protected Domain domain;
    protected Map<String, Double> weights;
    protected Map<String, IssueUtility> issueUtilities;

    protected Random rand;

    /**
     * This constructor allows you to create multiple utility functions for the same domain.
     * @param domain
     * @param randParams
     * @param seed
     */
    public LinearUtilityFunction(Domain domain, boolean randParams, long seed) {
        this.rand = new Random(seed);

        this.domain = domain;

        this.issueUtilities = generateIssueUtilities(randParams);
        this.weights = generateWeights(domain.getIssues(), randParams); /*we can't call issues() here, because it requires weights to have already been initialized.*/
    }

    private Map<String, IssueUtility> generateIssueUtilities(boolean randParams) {
        Map<String, IssueUtility> issueUtilities = new HashMap<>();
        for(String issue: domain) {
            IssueUtility iU = new IssueUtility(domain.getValues(issue),
                    randParams, rand.nextLong());
            issueUtilities.put(issue, iU);
        }

        return issueUtilities;
    }

    private Map<String, Double> generateWeights(List<String> issues,
                                                boolean randWeights) {
        Map<String, Double> result = new HashMap<String, Double>();

        if(randWeights) {
            // generate
            double s=0;
            for(String issue: issues) {
                double w = rand.nextDouble();
                result.put(issue, w);
                s += w;
            }

            // normalize
            for(String issue: issues) {
                double w = result.get(issue);
                w /= s;
                result.put(issue, w);
            }
        }
        else {
            double w = 1./issues.size();
            for(String issue: issues)
                result.put(issue, w);

        }

        return result;

    }

    public double getUtility(Bid bid) {
        double util = 0.;
        for(String issue: bid) {
            String value = bid.getValue(issue);
            double w = weights.get(issue),
                    uVal = getIssueUtility(issue, value);

            util += w * uVal;
        }

        return util;
    }

    public double getIssueUtility(String issue, String value) {
        return issueUtilities.get(issue).getUtility(value);
    }

    public double getMaxPossibleUtilWith(List<String> issuesUsed) {
        double u = 0.;
        for(String issue: issuesUsed)
            u += this.weights.get(issue);

        return u;
    }

    public List<String> issues() {
        return domain.getIssues();
    }

    public double getWeight(String issue) {
        return weights.get(issue);
    }

    public String getValueWithClosestIssueUtility(String issue, double uTarget) {
        return issueUtilities.get(issue).getValueWithClosestUtility(uTarget);
    }

    public Bid sample() {
        return domain.sample();
    }

    public List<String> getPossibleValues(String issue) {
        return domain.getValues(issue);
    }

    public Bid getMaxBid() {
        Bid bid = Bid.EMPTY_BID;
        for(String issue: issues()) {
            String maxValue = issueUtilities.get(issue).getMaxValue();
            bid = new Bid(bid, issue, maxValue);
        }

        return bid;
    }

    public double getMaxUtil() {
        return getUtility(getMaxBid());
    }

    public double variability(Bid newBid, Bid oldBid) {
        List<Double> dif = new ArrayList<>();
        for(String issue: domain) {
            String newValue = newBid.getValue(issue),
                    oldValue = oldBid.getValue(issue);

            double newU = getIssueUtility(issue, newValue),
                    oldU = getIssueUtility(issue, oldValue),
                    dU = newU - oldU;

            dif.add(dU);
        }

        return std(dif);
    }

    private double std(List<Double> values) {
        return Math.sqrt(var(values));
    }

    private double var(List<Double> values) {
        double m = mean(values);
        return values.stream().mapToDouble(v -> Math.pow(v-m, 2)).average().getAsDouble();
    }

    private double mean(List<Double> values) {
        return values.stream().mapToDouble(a->a).average().getAsDouble();
    }

    public int manhattanDistance(Bid bid1, Bid bid2) {
        double distance = 0.;
        for(String issue: domain) {
            String v1 = bid1.getValue(issue),
                    v2 = bid2.getValue(issue);

            double u1 = getIssueUtility(issue, v1),
                    u2 = getIssueUtility(issue, v2);

            distance += Math.abs(u1-u2);
        }

        return (int) Math.round(distance);
    }

    @Override
    public int compare(Bid b1, Bid b2) {
        return ((Double) getUtility(b1)).compareTo(getUtility(b2));
    }

}